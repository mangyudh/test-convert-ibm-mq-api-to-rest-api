# SRE TECHNICAL TEST - MANGGALA YUDHA

# Rate limitter used : 
- Kong API Gateway
- resilience4j

## Default setting :

### resilience4j
- limitForPeriod: 
Specifies the maximum number of requests allowed within a specific time period. In this example, the limit is set to 10 requests.
- limitRefreshPeriod: 
Defines the duration after which the limit will be reset. In this case, the limit will refresh every 1 second.
- timeoutDuration: 
The timeoutDuration property determines how long we are willing to wait for the rate limiter to allow us to proceed with a request. If the rate limit is exceeded and we don�t receive permission within the specified timeout duration, the request will be stopped and an error will be returned. This property helps control the maximum waiting time before timing out a request. Here, the timeout duration is set to 1 second.

### Kong API Gateway
- using rate-limitting plugins, The amount of HTTP requests the developer can make per second : 5 requests, more than this then will be blocked


## To run and test the application 
- Compile the source code : mvn clean package
- Run the docker build : docker build -t accelbyte/yudha .
- Run the containers : docker-compose up
- We need to wait for a while for the containers to finish the auto setup before proceeding with the next step
- Run the jmx/jmeter test attached (limitter.jmx)


## to change the rate limitting parameters:

### on the main app:
- Update the file ./extproperties/yudha.properties
- restart the accelbyteYudhaTest container and re-run the jmx test

### on the Kong API Gateway:
- login to Konga Dashboard using
    username : admin
    password : password1!
    navigate to Services->BackendTest->Routes->limitter->plugins-> click on rate-limitting , then update the value accordingly
- the changes would be in effect immediately


## links
- Submitted App using Kong API Gateway : http://localhost/api/v1/limitter/1 -> 1 here is only for @PathVariable and can be replaced with any integer http://localhost/api/v1/limitter/{id}
- Submitted App (direct) : http://localhost:8080/api/v1/limitter/1
- Kibana : http://localhost:5601 -> may need to search the logs manually
- 